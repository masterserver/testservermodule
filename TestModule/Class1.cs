﻿using Darari.Serialization.Abstractions;
using Darari.Serialization.Extensions;
using Darari.Server.Abstractions;
using Darari.Server.Abstractions.EntityFrameworkUtils;
using Microsoft.EntityFrameworkCore;

namespace TestModule;

public class TestModule: ModuleBase, IDatabaseProvider, IDisposable, IDatabaseProcessor
{
    public uint HandlerId => 0;
    private IDatabaseDispatcher _databaseDispatcher;
    
    public TestModule(IMessageDistributor messageDistributor, IConnectionsService connectionsService, IModulesService modulesService) : base(messageDistributor, connectionsService, modulesService)
    {
        messageDistributor.RegisterHandler<TestMessage>(HandlerId, 0, Callback);
    }

    private void Callback(IClient arg1, uint packageId, TestMessage arg2)
    {
        if (arg2.Message.StartsWith("register:"))
        {
            var userName = arg2.Message.Replace("register:", string.Empty);
            _databaseDispatcher.ProcessDatabase<TestModuleDbContext>(async db =>
            {
                var user = await db.Users.FirstOrDefaultAsync(x => x.Address == arg1.Address.ToString());
                if (user == null)
                {
                    await db.Users.AddAsync(new User() { Name = userName, Address = arg1.Address.ToString() });
                    await db.SaveChangesAsync();
                }
                else
                {
                    user.Name = userName;
                    db.Users.Update(user);
                }
            });
            
        }
        else
        {
            User? user = default;
            _databaseDispatcher.ProcessDatabase<TestModuleDbContext>(async db =>
            {
                user = await db.Users.FirstOrDefaultAsync(x => x.Address.Contains(arg1.Address.ToString()));
            });
            
            var message = arg2.Message;
            if (user != null)
            {
                message = user.Name + ": " + message;
            }
            else
            {
                message = "(Anonim)" + arg1.Address + ": " + message;
            }
            
            Console.WriteLine("Chat :: " + message);
            ConnectionsService.Broadcast(HandlerId, 0, new TestMessage() { Message = message });
        }
    }
    
    public void RegisterDatabase(IDatabaseVisitor visitor)
    {
        visitor.RegisterDatabase<TestModuleDbContext>();
    }

    public void SetDispatcher(IDatabaseDispatcher dispatcher)
    {
        _databaseDispatcher = dispatcher;
    }
    
    public void Dispose()
    {
        MessageDistributor.RemoveHandler(HandlerId);
    }
}

internal class TestMessage: ISerializable
{
    public string Message;
    
    public void Serialize(INetworkWriter writer)
    {
        writer.WriteString(Message);
    }

    public void Deserialize(INetworkReader reader)
    {
        Message = reader.ReadString();
    }
}
