﻿using Microsoft.EntityFrameworkCore;

namespace TestModule;

public sealed class TestModuleDbContext: DbContext
{
    private static bool _isCreated;
    
    public DbSet<User> Users { get; set; }

    public TestModuleDbContext(DbContextOptions<TestModuleDbContext> options) : base(options)
    {
        if (!_isCreated)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            _isCreated = true;
        }

    }
}